#!/bin/bash

set -e

echo "test 1"

TAG_NAME=$(./next_tag.sh)

echo "kos CI_COMMIT_SHA:" $CI_COMMIT_SHA
echo "kos TAG_NAME:" $TAG_NAME

curl --header "PRIVATE-TOKEN: ZtqZfsbiuJhWqjz9inns" -X POST "https://gitlab.com/api/v4/projects/16760949/repository/tags?tag_name=$TAG_NAME&ref=$CI_COMMIT_SHA"
