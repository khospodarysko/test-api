#!/usr/bin/env bash

MAJOR=$(git tag | grep "^[0-9]" | cut -d '.' -f 1 | sort | tail -1)
MINOR=$(git tag | grep "$MAJOR." | cut -d '.' -f 2 | sort | tail -1)
BUILD=$(($(git tag | grep "$MAJOR.$MINOR" | cut -d '.' -f 3 | sort -h | tail -1)+1))

echo "$MAJOR.$MINOR.$BUILD"
